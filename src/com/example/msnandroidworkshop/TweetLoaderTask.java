package com.example.msnandroidworkshop;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.widget.TextView;

public class TweetLoaderTask extends AsyncTask<String, Integer, String> {

	//TODO: this will probably be a list
	private TextView mViewToUpdate;
	private Context mContext;

	public TweetLoaderTask(TextView viewToUpdate, Context context) {
		mViewToUpdate = viewToUpdate;
		mContext = context;
	}

	@Override
	protected String doInBackground(String... params) {
		String username = params[0];
		String responseString = "";
		if(username == null)
			username = "";

		//get the actual tweet data here
		String uri = "https://api.twitter.com/1/statuses/user_timeline.json?include_entities=true&include_rts=true&screen_name=" + username + "&count=10";

		HttpClient tweetClient = new DefaultHttpClient();
		try
		{
			HttpResponse tweetResponse = tweetClient.execute(new HttpGet(uri));
			StatusLine statusLine = tweetResponse.getStatusLine();
			if(statusLine.getStatusCode() == HttpStatus.SC_OK)
			{
				BufferedReader reader = new BufferedReader(new InputStreamReader(tweetResponse.getEntity().getContent(), "UTF-8"));
				String json_string = reader.readLine();
				// Instantiate a JSON object from the request response
				JSONArray jArray = new JSONArray(json_string);
				List<String> tweets = new ArrayList<String>();
				for (int i=0; i < jArray.length(); i++)
				{
					JSONObject oneObject = jArray.getJSONObject(i);
					// Pulling items from the array
					String oneTweet = oneObject.getString("text");
					tweets.add(oneTweet);
				}
				
				//for now, append all tweets together
				for(int i=0; i <tweets.size(); i++)
				{
					responseString = responseString + tweets.get(i) + "\n\n";
				}
			}
			else if(statusLine.getStatusCode() == HttpStatus.SC_UNAUTHORIZED)
			{
				
				responseString = "UNAUTHORIZED";
			}
			else
			{
				tweetResponse.getEntity().getContent().close();
				System.out.println("Did not get an OK response: " + statusLine.getReasonPhrase());
				responseString = "Did not get an OK response: " + statusLine.getReasonPhrase();
			}
		}
		catch(JSONException e)
		{
			System.out.println("JSONException");
		}
		catch(ClientProtocolException e)
		{
			System.out.println("ClientProtocolException");
		}
		catch(IOException e)
		{
			System.out.println("IOException");
		}

		return responseString;
	}

	@Override
	protected void onPostExecute(String result) {
		if(result.equals("UNAUTHORIZED"))
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
			builder.setMessage("User has private Twitter Account")
			.setCancelable(false)
			.setNeutralButton("OK", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();					
				}
			});
			

			AlertDialog alert = builder.create();
			alert.show();
		}
		else if(mViewToUpdate != null) {
			mViewToUpdate.setText(result);
		}
		super.onPostExecute(result);
	}



}
